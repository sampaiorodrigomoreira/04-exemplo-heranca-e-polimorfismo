package br.com.itau;

public class Funcionario {
    protected String nome;
    protected String cpf;
    private double salario;

    public Funcionario(String nome, String cpf, double salario) {
        this.nome = nome;
        this.cpf = cpf;
        this.salario = salario;
    }

    public double getSalario(){
        return salario;
    }

    public double getBonificacao(){
        return salario * 0.05;
    }
}
