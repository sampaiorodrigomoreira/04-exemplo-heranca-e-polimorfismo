package br.com.itau;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Funcionario funcionario = new Funcionario(
                "Vini",
                "292929292",
                10000);
        System.out.println(funcionario.getBonificacao());

        Gerente gerente = new Gerente(
                "Jeff",
                "3993939393",
                20000,
                2);
        System.out.println(gerente.getBonificacao());
    }
}
